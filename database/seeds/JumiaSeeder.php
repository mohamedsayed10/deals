<?php

use Illuminate\Database\Seeder;

class JumiaSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'morocco',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'ghana',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'nigeria',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'kenya',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'ivory Coast',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'uganda',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'cameroon',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'senegal',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'tanzania',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'algeria',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'ethiopia',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'mozambique',
        ]);
        DB::table('manages')->insert([
            'company' => 'jumia',
            'country' => 'rwanda',
        ]);
    }

}
