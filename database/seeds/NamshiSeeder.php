<?php

use Illuminate\Database\Seeder;

class NamshiSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('manages')->insert([
            'company' => 'namshi',
            'country' => 'oman',
        ]);
        DB::table('manages')->insert([
            'company' => 'namshi',
            'country' => 'emirates',
        ]);
        DB::table('manages')->insert([
            'company' => 'namshi',
            'country' => 'saudi',
        ]);
        DB::table('manages')->insert([
            'company' => 'namshi',
            'country' => 'kuwait',
        ]);
        DB::table('manages')->insert([
            'company' => 'namshi',
            'country' => 'qatar',
        ]);
        DB::table('manages')->insert([
            'company' => 'namshi',
            'country' => 'bahrin',
        ]);
    }

}
