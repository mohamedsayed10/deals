<?php

use Illuminate\Database\Seeder;

class SouqSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('manages')->insert([
            'company' => 'souq',
            'country' => 'egypt',
        ]);
        DB::table('manages')->insert([
            'company' => 'souq',
            'country' => 'emirates',
        ]);
        DB::table('manages')->insert([
            'company' => 'souq',
            'country' => 'saudi',
        ]);
        DB::table('manages')->insert([
            'company' => 'souq',
            'country' => 'kuwait',
        ]);
    }

}
