<?php

use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('templates')->insert([
            'sliders' => 2,
            'route' => 'multiple',
        ]);
    }

}
