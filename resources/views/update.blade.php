@extends('layouts.admin')
@section('title','E-commerce Deals - Add new Page')
@section('css')
<!-- BEGIN PLUGINS CSS -->
<link rel="stylesheet" href="{{url('assets/globals/plugins/sweetalert2/sweetalert2.min.css')}}">
<link rel="stylesheet" href="{{url('assets/globals/css/plugins.css')}}">
<!-- END PLUGINS CSS -->
@endsection
@section('content')
<div class="page-header full-content bg-blue-grey">
    <div class="row">
        <div class="col-sm-6">
            <h1>Create New Pages</h1>
        </div><!--.col-->

    </div><!--.row-->
</div><!--.page-header-->


<!--.Affiliate Accounts Start-->
<div class="row"> 
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

    @endif
    <div class="col-md-12">
            
            <div class="" id="tab1_2">
                <form method="POST" id="multi_update">
                    <input type="hidden" value="{{ $multiple_page->id }}" id="page_id">
                    <div class="row example-row">
                        <div class="col-md-3">Title</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="multi_title" value="{{ $multiple_page->title }}" class="form-control inputmask inputmask-decimal" type="text">
                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->

                    <div class="row example-row">
                        <div class="col-md-3">description</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="multi_description" value="{{ $multiple_page->description }}" class="form-control inputmask inputmask-decimal-right" type="text">
                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    @foreach ($links as $key => $link)
                    <div class="row example-row">
                        <div class="col-md-3"> link {{ $key + 1 }}</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">

                                    <input name="urls[{{ $key }}]" class="form-control inputmask inputmask-phone urls urls{{ $key }}" value="{{ $link }}" type="text" placeholder="type http://www.souq.com/api/index.php">

                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->

                    <div class="row example-row">
                        <div class="col-md-3">Slider {{ $key + 1 }} - {{ $limit[$key] }} items</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <select name="slider[{{ $key }}]" class="form-control sliders sliders{{ $key }}">
                                
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="25">25</option>
                                <option value="30">30</option>							
                            </select>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->


                    <div class="row example-row">
                        <div class="col-md-3">Affiliate Account {{ $key + 1 }}</div>
                        <!--.col-md-3-->

                        <div class="col-md-9">
                            <select class="form-control multi_affiliates multi_affiliates{{ $key }}" name="affiliate[{{ $key }}]">
                                @foreach( $affiliates as $affiliate )
                                @if($affiliate->id == $affiliate_arr[$key])
                                <option value="{{ $affiliate->id }}" selected="">{{ $affiliate->name }}</option>
                                @else
                                <option value="{{ $affiliate->id }}">{{ $affiliate->name }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>


                        <!--.col-md-9-->
                    </div>
                    @endforeach

                    <button type="submit" class="btn btn-success btn-ripple">Create Page</button>
                </form>

                <!--.Tab 2  Multi product -->
            </div>
            


    </div>
    <!--.Affiliate Accounts Start-->

    @endsection


    @section('menu')
<ul>
    <li>
        <a href="{{ url('pages') }}">Pages</a>
    </li>
    <li>
        <a href="{{ url('new') }}">Create Pages</a>

    </li>				<li>
        <a href="{{ url('users') }}">User Accounts</a>

    </li>
    <li>
        <a href="{{ url('affiliate') }}">Affiliate Accounts </a>

    </li>

</ul>
    @endsection
    @section('js')
    <!-- PLUGINS INITIALIZATION AND SETTINGS -->
    <!-- for IE support -->
    <script src="{{url('assets/globals/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{url('assets/add.js')}}"></script>
    <!-- END PLUGINS INITIALIZATION AND SETTINGS -->
    @endsection
    @section('init')
    <script>
var csrf = '{{ csrf_token() }}';
    </script>
    <!-- BEGIN INITIALIZATION-->
    <script>
        $(document).ready(function () {
            Pleasure.init();
            Layout.init();
        });
    </script>
    <!-- END INITIALIZATION-->
    @endsection
