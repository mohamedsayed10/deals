@extends('layouts.admin')

@section('title','E-commerce Deals - Pages')
@section('css')
<!-- BEGIN PLUGINS CSS -->
<link rel="stylesheet" href="assets/globals/plugins/datatables/media/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="assets/globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="assets/globals/css/plugins.css">
<link rel="stylesheet" href="assets/globals/plugins/sweetalert2/sweetalert2.min.css">

<!-- END PLUGINS CSS -->
@endsection
@section('content')
<div class="page-header full-content bg-blue-grey">
    <div class="row">
        <div class="col-sm-6">
            <h1>pages</h1>
        </div>
        <!--.col-->
    </div>
    <!--.row-->
</div>
<!--.page-header-->
<div class="row">
    <table class="display datatables-crud dataTable" id="pages_view">
        <thead>
            <tr>
                <th style="color: #3e50b4;">ID</th>
                <th class="col-xs-3">
                    <span style="color: #3e50b4; display:block;" class="title">Name</span>
                    <span class="caption">disc</span>
                </th>

                <th class="col-xs-2">
                    <span style="color: #3e50b4; display:block;" class="title">affiliate acc</span>
                    <span class="caption">company</span>
                </th>

                <th class="col-xs-3">
                    <span style="color: #3e50b4;display:block;" class="title">template</span>
                    <span class="caption">link</span>
                </th>

                <th class="col-xs-2">
                    <span style="color: #3e50b4; display:block;" class="title">views</span>
                    <span class="caption">uniq views</span>
                </th>
                <th class="col-xs-1" style="color: #3e50b4;">
                    <span style="color: #3e50b4; display:block;" class="title">controls</span>
                    <span class="caption"> edit / delete</span> </th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $multiple_items as $multiples )

            <tr>
                <td class="item_id">{{ $multiples['item']['item']->id }}</td>
                <td>
                    <a target="_blank" href="{{ url($multiples['item']['item']->id.'-'.$multiples['item']['item']->title) }}"> <span>{{ $multiples['item']['item']->title }}</span> </a>
                    <br>
                    <span class="caption">{{ $multiples['item']['item']->description }}</span>
                </td>

                <td>
                    @foreach($multiples['affiliates'] as $affiliate)
                    <div>(
                        <span>{{ $affiliate->name }}</span>
                        <span class="caption">{{ $affiliate->company }}</span>)
                    </div>
                    @endforeach

                </td>

                <td class="type">
                    <span>multiple</span>
                    <br>
                    @foreach($multiples['urls'] as $url)
                    <a href="{{ $url }}" class="caption" target="_blank">{{ $url }}</a>
                    @endforeach
                </td>

                <td>
                    <span>{{ $multiples['item']['item']->views }}</span>
                    <br>
                    <span class="caption">{{ $multiples['item']['views'] }}</span>
                </td>
                
                <td>
                    <a href="ShowMultiple/{{$multiples['item']['item']->id}}#tab_tab1_2" class="btn btn-primary btn-xs  btn-ripple edit-btn">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>

                    <button class="btn btn-danger btn-xs dt-delete btn-ripple"><span class="glyphicon glyphicon-trash"></span></button>
                </td>
            </tr>

            @endforeach
            @foreach( $single_pages as $single )


            <tr>
                <td class="item_id">{{ $single['item']->id }}</td>
                <td>
                    <a target="_blank" href="{{ url('single',$single['item']->id.'-'.$single['item']->title) }}"> <span>{{ $single['item']->title }}</span></a>
                    <br>
                    <span class="caption">{{ $single['item']->description }}</span>
                </td>

                <td>

                    <div>(
                        <span>{{ $single['item']->Affiliate->name }}</span>
                        <span class="caption">{{ $single['item']->Affiliate->company }}</span>)
                    </div>


                </td>

                <td class="type">
                    <span class="type">single</span>
                    <br>
                    <a href="{{ $single['item']->url }}" class="caption" target="_blank">{{ $single['item']->url }}</a>

                </td>

                <td>
                    <span>{{ $single['item']->views }}</span>
                    <br>
                    <span class="caption">{{ $single['views'] }}</span>
                </td>

                <td>
                    <a href="ShowSingle/{{$single['item']->id}}" class="btn btn-primary btn-xs  btn-ripple">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>

                    <button class="btn btn-danger btn-xs dt-delete btn-ripple"><span class="glyphicon glyphicon-trash"></span></button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!--.edit page modal -->

<div class="modal fade full-height from-right in" id="edit_page" tabindex="-1" role="dialog" aria-hidden="false" style=" ">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title " style=" color:#3e50b4;">edit page</h4>
            </div>

            <div class="modal-body">

                <div class="row example-row">
                    <div class="col-md-3">Title</div>
                    <!--.col-md-3-->
                    <div class="col-md-9">
                        <div class="inputer">
                            <div class="input-wrapper">
                                <input class="form-control inputmask inputmask-decimal page_title" type="text">
                            </div>
                        </div>
                    </div>
                    <!--.col-md-9-->
                </div>
                <!--.row-->

                <div class="row example-row">
                    <div class="col-md-3">description</div>
                    <!--.col-md-3-->
                    <div class="col-md-9">
                        <div class="inputer">
                            <div class="input-wrapper">
                                <input  class="form-control inputmask inputmask-decimal-right page_description" type="text">
                            </div>
                        </div>
                    </div>
                    <!--.col-md-9-->
                </div>
                <!--.row-->
                <div class="multiple">

                <div class="row example-row">
                    <div class="col-md-3"> link</div>
                    <!--.col-md-3-->
                    <div class="col-md-9">
                        <div class="inputer">
                            <div class="input-wrapper page_links">
                                <input class="form-control inputmask inputmask-phone" type="text" placeholder="ex: http://www.souq.com/api/index.php">
                            </div>
                        </div>
                    </div>
                    <!--.col-md-9-->
                </div>
                <!--.row-->


                <div class="row example-row">
                    <div class="col-md-3">Select Accounts</div>
                    <!--.col-md-3-->
                    <div class="col-md-9">
                        <select class="selecter affiliates">
                            <option>Affilate Account</option>
                            <option>E-deals Account</option>

                        </select>
                    </div>
                    <!--.col-md-9-->
                </div>
                <!--.row-->

                <div class="row example-row">
                    <div class="col-md-3">Product counts</div>
                    <!--.col-md-3-->
                    <div class="col-md-9">
                        <select  class="selecter">
                            <option>5</option>
                            <option>10</option>
                            <option>15</option>
                            <option>20</option>
                            <option>25</option>
                            <option>30</option>
                        </select>
                    </div>
                    <!--.col-md-9-->
                </div>
                <!--.row-->
                
                </div>

                <div class="modal-footer">

                    <button class="btn btn-success btn-ripple">confirm edits</button>
                    <a href="#" class="btn btn-blue btn-ripple">Preview</a>

                </div>
            </div>

        </div>
    </div>
</div>




@section('menu')
<ul>
    <li data-open-after="true">
        <a href="{{ url('pages') }}">Pages</a>
    </li>
    <li>
        <a href="{{ url('new') }}">Create Pages</a>

    </li>				<li>
        <a href="{{ url('users') }}">User Accounts</a>

    </li>
    <li>
        <a href="{{ url('affiliate') }}">Affiliate Accounts </a>

    </li>

</ul>
@endsection
@section('js')
<script>
    var csrf = '{{ csrf_token() }}';
</script>
<!-- BEGIN PLUGINS AREA -->
<script src="assets/globals/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="assets/globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PLUGINS AREA -->

<!-- PLUGINS INITIALIZATION AND SETTINGS -->
<script src="assets/globals/plugins/sweetalert2/sweetalert2.min.js"></script>

<script src="assets/globals/scripts/tables-datatables-editor.js"></script>
<script src="assets/add.js"></script>
<!-- END PLUGINS INITIALIZATION AND SETTINGS -->
@endsection
@section('init')
<!-- BEGIN INITIALIZATION-->
<script>
    $(document).ready(function () {
        Pleasure.init();
        Layout.init();
        TablesDataTablesEditor_pages_view.init();
    });

</script>
<!-- END INITIALIZATION-->
@endsection
@endsection
