<!DOCTYPE html>
<html>
    <head>
        <!-- meta tags -->
        <meta charset="utf-8"/>
        <meta content="telephone=no" name="format-detection"/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
            <meta content="Responsive and clean html template design for any kind of ecommerce webshop" name="description">
                <!-- meta tags -->
                <!-- title and icon -->
                <link href="image/favicon.png" rel="icon"/>
                <title>
                    {{ $item['title'] }}
                </title>
                <!-- title and icon -->
                <!-- CSS Part Start-->
                <link href="{{ url('js/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
                <link href="{{ url('css/stylesheet.css') }}" rel="stylesheet" type="text/css"/>
                <!-- CSS Part End-->
            </meta>
        </meta>
        <style type="text/css">
            .descrption > div{
    max-width: 650px;
    max-height: 350px;
    overflow-x: hidden;
        overflow-y: scroll;

}
::-webkit-scrollbar {
    width: 12px;
}
 
/* Track */
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 10px;
    border-radius: 10px;
    background: rgb(132, 128, 162);
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
}
::-webkit-scrollbar-thumb:window-inactive {
    background: rgb(132, 128, 162);
}
        .title{
        font-size: 35px!important;
        }
        </style>
    </head>
    <body>
        <div class="wrapper-wide">
            <!-- Top Bar Start-->
            <div id="header">
                <nav class="htop" id="top">
                    <div class="container">
                        <!-- Logo Start -->
                        <div class="col-lg-4 col-md-4 col-xs-8 col">
                            <div id="logo">
                                <a href="index.html">
                                    <img alt="E-deals" class="img-responsive" src="{{url('image/logo.png')}}" title="E-deals"/>
                                </a>
                            </div>
                        </div>
                        <!-- Logo End -->
                        <!-- language switch Start -->
                        <div class="col-lg-2 col-md-4 col-xs-4 pull-right">
                            <!--<button class="btn btn-link language-select" type="button" name="GB">
                                <img src="image/flags/gb.png" alt="English" title="English" /> English
                            </button>-->
                            <button class="btn btn-link " name="GB" type="button">
                                <img alt="Arabic" src="{{url('image/flags/eg.png')}}" title="Arabic"/>
                                العربية
                            </button>
                        </div>
                        <!-- language switch end -->
                    </div>
                </nav>
            </div>
            <!-- Top Bar End-->
            <!-- page content Start -->
            <div class="container">
                <div class="row">
                    <!--product show start -->
                    <div class="row product-info">
                        <!--photos Start (eleveate zoom plugin)-->
                        <div class="col-sm-5 col-xs-12">
                            <img class="col-xs-12" data-zoom-image="{{ $item['images']['0'] }}" id="img_01" src="{{ $item['images']['0'] }}"/>
                            <div id="gal1">
                                @foreach($item['images'] as $image)
                                <a class="col-xs-3 img_01" data-image="{{ $image }}" data-zoom-image="{{ $image }}" href="#">
                                    <img class="col-xs-12" id="img_01" src="{{ $image }}"/>
                                </a>
                                @endforeach
                            </div>
                        </div>
                        <!--photos end (eleveate zoom plugin)-->
                        <!-- product info start -->
                        <div class="col-sm-7 col-xs-12" style="background:#211d3d;">
                            <ul class="price-box">
                                <li>
                                    <h2 class="title">
                                        {{ $item['title'] }}
                                    </h2>
                                </li>
                                <li>
                                    <h1 class="price">
                                        {{ $item['price'] }}
                                    </h1>
                                    <span class="currency">
                                        pound
                                        <br>
                                            Egyptian
                                        </br>
                                    </span>
                                </li>
                                <li>
                                    <hr>
                                        <div class="descrption">
                                            <b>
                                                Description
                                            </b>
                                            {!! $item['descrption'] !!}
                                        </div>
                                    </hr>
                                </li>
                            </ul>
                        </div>
                        <!-- product info end -->
                        <div class=" col-sm-7 col-xs-12" style="padding:0;">
                            <a class="call-to-action" href="{{ $item['link'] }}" target="_blank">
                                Buy it now
                            </a>
                        </div>
                    </div>
                    <!--product show End -->
                </div>
            </div>
            <!-- page content end -->
        </div>
        <!-- JS Part Start-->
        <script src="{{url('js/jquery-2.1.1.min.js')}}" type="text/javascript">
        </script>
        <script src="{{url('js/jquery.elevateZoom-3.0.8.min.js')}}" type="text/javascript">
        </script>
        <script type="text/javascript">
            // Elevate Zoom for Product Page image
        $("#img_01").elevateZoom({
            gallery: 'gal1',
            responsive: true,
            zoomType: 'inner',
            cursor: 'pointer',
            galleryActiveClass: 'active',
            loadingIcon: '{{url('image/progress.gif') }}',
        });

        //pass the images to Fancybox
        $("#img_01").bind("click", function(e) {
            var ez = $('#img_01').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });
        </script>
        <!-- JS Part End-->
        <style>
        </style>
    </body>
</html>