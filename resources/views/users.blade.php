@extends('layouts.admin')

@section('title','E-commerce Deals - Users')
@section('css')
<!-- BEGIN PLUGINS CSS -->
<link rel="stylesheet" href="assets/globals/plugins/datatables/media/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="assets/globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="assets/globals/plugins/sweetalert2/sweetalert2.min.css">

<link rel="stylesheet" href="assets/globals/css/plugins.css">
<!-- END PLUGINS CSS -->
@endsection
@section('content')
<div class="page-header full-content bg-blue-grey">
    <div class="row">
        <div class="col-sm-6">
            <h1>User Accounts</h1>
        </div>
        <!--.col-->
    </div>
    <!--.row-->
</div>

<div class="row">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

    @endif
    <table class="display datatables-crud dataTable" id="users_acounts_table">
        <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>E-Mail</th>
                <th>Password</th>
                <th>Confirm Password</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>

        <tbody>
            @foreach( $users as $user )
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>*******</td>
                <td>*******</td>
                <td>
                    <button class="btn btn-primary btn-xs dt-edit btn-ripple"><span class="glyphicon glyphicon-pencil"></span></button>
                </td>
                <td>
                    <button class="btn btn-danger btn-xs dt-delete btn-ripple"><span class="glyphicon glyphicon-trash"></span></button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@section('menu')
<ul>
    <li>
        <a href="{{ url('pages') }}">Pages</a>
    </li>
    <li>
        <a href="{{ url('new') }}">Create Pages</a>

    </li>				
    <li data-open-after="true">
        <a href="{{ url('users') }}">User Accounts</a>

    </li>
    <li>
        <a href="{{ url('affiliate') }}">Affiliate Accounts </a>

    </li>

</ul>
@endsection
@section('js')
<script>
    var csrf = '{{ csrf_token() }}';
</script>
<!-- BEGIN PLUGINS AREA -->
<script src="assets/globals/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="assets/globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/globals/plugins/sweetalert2/sweetalert2.min.js"></script>

<!-- END PLUGINS AREA -->

<!-- PLUGINS INITIALIZATION AND SETTINGS -->
<script src="assets/globals/scripts/tables-datatables-editor.js"></script>
<!-- END PLUGINS INITIALIZATION AND SETTINGS -->
@endsection
@section('init')
<!-- BEGIN INITIALIZATION-->
<script>
    $(document).ready(function () {
        Pleasure.init();
        Layout.init();
        TablesDataTablesEditor_users_accounts.init();
    });

</script>
<!-- END INITIALIZATION-->
@endsection
@endsection
