@extends('layouts.admin')
@section('title','E-commerce Deals - Add new Page')
@section('css')
<!-- BEGIN PLUGINS CSS -->
<link rel="stylesheet" href="assets/globals/plugins/sweetalert2/sweetalert2.min.css">
<link rel="stylesheet" href="assets/globals/css/plugins.css">
<!-- END PLUGINS CSS -->
@endsection
@section('content')
<div class="page-header full-content bg-blue-grey">
    <div class="row">
        <div class="col-sm-6">
            <h1>Create New Pages</h1>
        </div><!--.col-->

    </div><!--.row-->
</div><!--.page-header-->


<!--.Affiliate Accounts Start-->
<div class="row"> 
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

    @endif
    <div class="col-md-12">

        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#tab1_1" data-toggle="tab" aria-expanded="true">single product page</a></li>
            <li class=""><a href="#tab1_2" data-toggle="tab" aria-expanded="false">multi product page</a></li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="tab1_1">
                <form id="single">
                    <div class="row example-row">
                        <div class="col-md-3">Title</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="single_title" class="form-control inputmask inputmask-decimal" type="text">
                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <div class="row example-row">
                        <div class="col-md-3">description</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="single_description" class="form-control inputmask inputmask-decimal-right" type="text">
                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <div class="row example-row">
                        <div class="col-md-3">Link</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="single_url" class="form-control inputmask inputmask-phone" type="text" placeholder="type http://">

                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <div class="row example-row">
                        <div class="col-md-3">affiliate Account</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <select class="selecter" id="single_affiliate">
                                @foreach( $affiliates as $affiliate )
                                <option value="{{ $affiliate->id }}">{{ $affiliate->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <!--.row-->
                    <div class="row example-row">
                        <div class="col-md-3">Select Company</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <select class="selecter" id="single_company">
                                <option value="souq">SOUQ</option>
                                <option value="namshi">NAMSHI</option>
                                <option value="jumia">JUMIA</option>
                            </select>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <button class="btn btn-success btn-ripple" type="submit">Creat Page</button>
                </form>
            </div>
            <!--.Tab 1  Single product -->

            <div class="tab-pane" id="tab1_2">
                <form id="multi">
                    <div class="row example-row">
                        <div class="col-md-3">Title</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="multi_title" class="form-control inputmask inputmask-decimal" type="text">
                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->

                    <div class="row example-row">
                        <div class="col-md-3">description</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="multi_description" class="form-control inputmask inputmask-decimal-right" type="text">
                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    @for( $x = 0; $x < $template->sliders; $x++ )

                    <div class="row example-row">
                        <div class="col-md-3"> link {{ $x + 1 }}</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">

                                    <input  name="urls[{{ $x }}]" class="form-control inputmask inputmask-phone urls urls{{ $x }}" type="text" placeholder="type http://www.souq.com/api/index.php">

                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->

                    <div class="row example-row">
                        <div class="col-md-3">Slider {{ $x + 1 }} </div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <select name="slider[{{ $x }}]" class="form-control sliders sliders{{ $x }}">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="25">25</option>
                                <option value="30">30</option>							
                            </select>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->


                    <div class="row example-row">
                        <div class="col-md-3">Affiliate Account {{ $x + 1 }}</div>
                        <!--.col-md-3-->

                        <div class="col-md-9">
                            <select class="form-control multi_affiliates multi_affiliates{{ $x }}" name="affiliate[{{ $x }}]">
                                @foreach( $affiliates as $affiliate )
                                <option value="{{ $affiliate->id }}">{{ $affiliate->name }}</option>
                                @endforeach
                            </select>
                        </div>


                        <!--.col-md-9-->
                    </div>
                    @endfor

                    <button type="submit" class="btn btn-success btn-ripple">Create Page</button>
                </form>

                <!--.Tab 2  Multi product -->
            </div>
        </div>

    </div>
    <!--.Affiliate Accounts Start-->

    @endsection


    @section('menu')
<ul>
    <li>
        <a href="{{ url('pages') }}">Pages</a>
    </li>
    <li data-open-after="true">
        <a href="{{ url('new') }}">Create Pages</a>

    </li>				<li>
        <a href="{{ url('users') }}">User Accounts</a>

    </li>
    <li>
        <a href="{{ url('affiliate') }}">Affiliate Accounts </a>

    </li>

</ul>
    @endsection
    @section('js')
    <!-- PLUGINS INITIALIZATION AND SETTINGS -->
    <!-- for IE support -->
    <script src="assets/globals/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="assets/add.js"></script>
    <!-- END PLUGINS INITIALIZATION AND SETTINGS -->
    @endsection
    @section('init')
    <script>
var csrf = '{{ csrf_token() }}';
    </script>
    <!-- BEGIN INITIALIZATION-->
    <script>
        $(document).ready(function () {
            Pleasure.init();
            Layout.init();
        });
    </script>
    <!-- END INITIALIZATION-->
    @endsection
