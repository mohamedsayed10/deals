<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>@yield('title')</title>

        <meta name="description" content="@yield('descrption')">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <!-- BEGIN CORE CSS -->
        <link rel="stylesheet" href="{{ url('assets/admin1/css/admin1.css') }}">
        <link rel="stylesheet" href="{{ url('assets/globals/css/elements.css') }}">
        <!-- END CORE CSS -->


        <!-- BEGIN PLUGINS CSS -->
        @yield('css')
        <!-- END PLUGINS CSS -->


        <!-- BEGIN SHORTCUT AND TOUCH ICONS -->
        <link rel="shortcut icon" href="{{ url('assets/globals/img/icons/favicon.ico') }}">
        <link rel="apple-touch-icon" href="{{ url('assets/globals/img/icons/apple-touch-icon.png') }}">
        <!-- END SHORTCUT AND TOUCH ICONS -->
        <style>
            .loader:before,
            .loader:after,
            .loader {
                border-radius: 50%;
                width: 2.5em;
                height: 2.5em;
                -webkit-animation-fill-mode: both;
                animation-fill-mode: both;
                -webkit-animation: load7 1.8s infinite ease-in-out;
                animation: load7 1.8s infinite ease-in-out;
            }
            .loader {
                color: #ffffff;
                font-size: 10px;
                margin: 80px auto;
                position: relative;
                text-indent: -9999em;
                -webkit-transform: translateZ(0);
                -ms-transform: translateZ(0);
                transform: translateZ(0);
                -webkit-animation-delay: -0.16s;
                animation-delay: -0.16s;
            }
            .loader:before {
                left: -3.5em;
                -webkit-animation-delay: -0.32s;
                animation-delay: -0.32s;
            }
            .loader:after {
                left: 3.5em;
            }
            .loader:before,
            .loader:after {
                content: '';
                position: absolute;
                top: 0;
            }
            @-webkit-keyframes load7 {
                0%,
                80%,
                100% {
                    box-shadow: 0 2.5em 0 -1.3em;
                }
                40% {
                    box-shadow: 0 2.5em 0 0;
                }
            }
            @keyframes load7 {
                0%,
                80%,
                100% {
                    box-shadow: 0 2.5em 0 -1.3em;
                }
                40% {
                    box-shadow: 0 2.5em 0 0;
                }
            }

        </style>
        <script src="{{ url('assets/globals/plugins/modernizr/modernizr.min.js') }}"></script>
    </head>
    <body>
        <div class="nav-bar-container">

            <!-- BEGIN ICONS -->
            <div class="nav-menu">
                <div class="hamburger">
                    <span class="patty"></span>
                    <span class="patty"></span>
                    <span class="patty"></span>
                    <span class="patty"></span>
                    <span class="patty"></span>
                    <span class="patty"></span>
                </div><!--.hamburger-->
            </div><!--.nav-menu-->



            <div class="nav-bar-border"></div><!--.nav-bar-border-->

            <!-- BEGIN OVERLAY HELPERS -->
            <div class="overlay">
                <div class="starting-point">
                    <span></span>
                </div><!--.starting-point-->
                <div class="logo">E-deals</div><!--.logo-->
            </div><!--.overlay-->

            <div class="overlay-secondary"></div><!--.overlay-secondary-->
            <!-- END OF OVERLAY HELPERS -->

        </div><!--.nav-bar-container-->
        <div class="content">
            @yield('content')
        </div>
        <div class="layer-container">

            <!-- BEGIN MENU LAYER -->
            <div class="menu-layer">
                @yield('menu')
            </div>
        </div>


        <!-- BEGIN GLOBAL AND THEME VENDORS -->
        <script src="{{ url('assets/globals/js/global-vendors.js') }}"></script>
        <!-- END GLOBAL AND THEME VENDORS -->

        <!-- BEGIN PLUGINS AREA -->
        @yield('js')
        <!-- END PLUGINS AREA -->

        <!-- PLEASURE -->
        <script src="{{ url('assets/globals/js/pleasure.js') }}"></script>
        <!-- ADMIN 1 -->
        <script src="{{ url('assets/admin1/js/layout.js') }}"></script>
        @yield('init')



        <div class="modal-backdrop fade in" style="display:none">
            <div class="loader">Loading...</div>
        </div>
    </body>
