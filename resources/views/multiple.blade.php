<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta content="telephone=no" name="format-detection"/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
            <link href="image/favicon.png" rel="icon"/>
            <title>
                {{ $PageTitle }}
            </title>
            <meta content="{{ $PageDescription }}" name="description">
                <!-- CSS Part Start-->
                <link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
                <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
                <link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
                <link href="css/stylesheet-skin3.css" rel="stylesheet" type="text/css"/>
                <link href="css/owl.carousel.css" rel="stylesheet" type="text/css"/>
                <link href="css/owl.transitions.css" rel="stylesheet" type="text/css"/>
                <!-- CSS Part End-->
            </meta>
        </meta>
    </head>
    <body>
        <div class="wrapper-wide">
        <!-- Top Bar Start-->
        <div id="header">
            <nav id="top" class="htop">
                <div class="container">

                    <!-- Logo Start -->
                    <div class="col-lg-4 col-md-4 col-xs-8 col">
                        <div id="logo">
                            <a href="index.html"><img class="img-responsive" src="image/logo.png" title="E-deals" alt="E-deals" /></a>
                        </div>
                    </div>
                    <!-- Logo End -->

                    <!-- language switch Start -->
                    <div class="col-lg-2 col-md-4 col-xs-4 pull-right">

                        <!--<button class="btn btn-link language-select" type="button" name="GB">
                                <img src="image/flags/gb.png" alt="English" title="English" /> English
                            </button>-->

                        <button class="btn btn-link " type="button" name="GB">
                            <img src="image/flags/eg.png" alt="Arabic" title="Arabic" /> العربية
                        </button>

                    </div>
                    <!-- language switch end -->

                </div>
            </nav>
        </div>
        <!-- Top Bar End-->
            <div class="container-fluid">
                <div class="row">
                    <!--Middle Part Start-->
                    <div class="col-xs-12" id="content">
                        @foreach ( $exLink as $key1 => $company )
                        <h3 class="subtitle">
                            {{ $company['company'] }}
                        </h3>
                        <div class="owl-carousel nxt latest_category_carousel">
                            @foreach ( $company['products'] as $key => $product )
                            @if(isset($product['link']))
                            <div class="product-thumb clearfix">
                                <div class="image">
                                    <a href="{{ $product['link'] }}{{ $NumAffiliate[$key1]->url }}" target="_blank">
                                        <img alt="{{ $product['title'] }}" class="img-responsive" src="{{ $product['image'] }}" title="{{ $product['title'] }}"/>
                                    </a>
                                </div>
                                <div class="caption">
                                    <h4>
                                        <a href="{{ $product['link'] }}{{ $NumAffiliate[$key1]->url }}" target="_blank">
                                            {{ $product['title'] }}
                                        </a>
                                    </h4>
                                    <p class="price2">
                                        {{ $product['price'] }}
                                    </p>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!--Footer Start-->
        <footer id="footer">
            <div class="fpart-second">
                <div class="container">
                    <div class="clearfix" id="powered">
                        <div class="powered_text pull-left flip">
                            <p>
                                E-commerce Deals © 2016
                            </p>
                        </div>
                    </div>
                    <div id="back-top">
                        <a class="backtotop" data-toggle="tooltip" href="javascript:void(0)" title="Back to Top">
                            <i class="fa fa-chevron-up">
                            </i>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
        <!--Footer End-->
        <!-- JS Part Start-->
        <script src="js/jquery-2.1.1.min.js" type="text/javascript">
        </script>
        <script src="js/jquery.easing-1.3.min.js" type="text/javascript">
        </script>
        <script src="js/jquery.dcjqaccordion.min.js" type="text/javascript">
        </script>
        <script src="js/owl.carousel.min.js" type="text/javascript">
        </script>
        <script src="js/custom.js" type="text/javascript">
        </script>
        <!-- JS Part End-->
        <script>
            $(document).ready(function() {

                $("#owl-example").owlCarousel();

            });
        </script>
    </body>
</html>