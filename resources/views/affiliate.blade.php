@extends('layouts.admin')
@section('title','E-commerce Deals - Affiliate')
@section('css')
<!-- BEGIN PLUGINS CSS -->
<link rel="stylesheet" href="assets/globals/plugins/datatables/media/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="assets/globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="assets/globals/plugins/sweetalert2/sweetalert2.min.css">

<link rel="stylesheet" href="assets/globals/css/plugins.css">
<!-- END PLUGINS CSS -->
@endsection
@section('content')
<div class="page-header full-content bg-blue-grey">
    <div class="row">
        <div class="col-sm-6">
            <h1>Affiliate Accounts</h1>
        </div><!--.col-->

    </div><!--.row-->
</div><!--.page-header-->


<!--.Affiliate Accounts Start-->
<div class="row"> 
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

    @endif
    <table class="display datatables-crud dataTable" id="afiiliate_accounts_table">
        <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>URL</th>
                <th>Company</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $affiliates as $affiliate )
            <tr>
                <td>{{ $affiliate->id }}</td>
                <td>{{ $affiliate->name }}</td>
                <td>{{ $affiliate->url }}</td>
                <td>{{ $affiliate->company }}</td>
                <td><button class="btn btn-primary btn-xs dt-edit btn-ripple"><span class="glyphicon glyphicon-pencil"></span></button></td>
                <td><button class="btn btn-danger btn-xs dt-delete btn-ripple"><span class="glyphicon glyphicon-trash"></span></button></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!--.Affiliate Accounts Start-->

@endsection


@section('menu')
<ul>
    <li>
        <a href="{{ url('pages') }}">Pages</a>
    </li>
    <li>
        <a href="{{ url('new') }}">Create Pages</a>

    </li>				<li>
        <a href="{{ url('users') }}">User Accounts</a>

    </li>
    <li data-open-after="true">
        <a href="{{ url('affiliate') }}">Affiliate Accounts </a>

    </li>

</ul>
@endsection
@section('js')

<!-- BEGIN PLUGINS AREA -->
<script src="assets/globals/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="assets/globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/globals/plugins/sweetalert2/sweetalert2.min.js"></script>

<!-- END PLUGINS AREA -->

<!-- PLUGINS INITIALIZATION AND SETTINGS -->
<script src="assets/globals/scripts/tables-datatables-editor.js"></script>
<!-- END PLUGINS INITIALIZATION AND SETTINGS -->
@endsection
@section('init')
<script>
var csrf = '{{ csrf_token() }}';
</script>
<!-- BEGIN INITIALIZATION-->
<script>
    $(document).ready(function () {
        Pleasure.init();
        Layout.init();
        TablesDataTablesEditor_affiliate_accounts.init();
    });
</script>
<!-- END INITIALIZATION-->
@endsection
