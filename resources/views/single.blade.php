@extends('layouts.admin')
@section('title','E-commerce Deals - Add new Page')
@section('css')
<!-- BEGIN PLUGINS CSS -->
<link rel="stylesheet" href="{{url('assets/globals/plugins/sweetalert2/sweetalert2.min.css')}}">
<link rel="stylesheet" href="{{url('assets/globals/css/plugins.css')}}">
<!-- END PLUGINS CSS -->
@endsection
@section('content')
<div class="page-header full-content bg-blue-grey">
    <div class="row">
        <div class="col-sm-6">
            <h1>Create New Pages</h1>
        </div><!--.col-->

    </div><!--.row-->
</div><!--.page-header-->


<!--.Affiliate Accounts Start-->
<div class="row"> 
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

    @endif
    <div class="col-md-12">

            
            <div class="tab-pane active" id="tab1_1">
                <form id="single_update" method="POST">
                    <input type="hidden" value="{{ $single_page->id }}" id="single_id">
                    <div class="row example-row">
                        <div class="col-md-3">Title</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="single_title" value="{{ $single_page->title }}" class="form-control inputmask inputmask-decimal" type="text">
                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <div class="row example-row">
                        <div class="col-md-3">description</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="single_description" value="{{ $single_page->description }}" class="form-control inputmask inputmask-decimal-right" type="text">
                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <div class="row example-row">
                        <div class="col-md-3">Link</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input id="single_url" value="{{ $single_page->url }}" class="form-control inputmask inputmask-phone" type="text" placeholder="type http://">

                                </div>
                            </div>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <div class="row example-row">
                        <div class="col-md-3">affiliate Account</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <select class="selecter" id="single_affiliate">
                                @foreach( $affiliates as $affiliate )
                                @if($affiliate->id == $single_page->affiliate_id)
                                <option value="{{ $affiliate->id }}" selected="">{{ $affiliate->name }}</option>
                                @else
                                 <option value="{{ $affiliate->id }}">{{ $affiliate->name }}</option>
                                @endif
                               
                                @endforeach
                            </select>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <!--.row-->
                    <div class="row example-row">
                        <div class="col-md-3">Select Company - {{ $single_page->company }}</div>
                        <!--.col-md-3-->
                        <div class="col-md-9">
                            <select class="selecter" id="single_company">
                                <option value="souq">SOUQ</option>
                                <option value="namshi">NAMSHI</option>
                                <option value="jumia">JUMIA</option>
                            </select>
                        </div>
                        <!--.col-md-9-->
                    </div>
                    <!--.row-->
                    <button class="btn btn-success btn-ripple" type="submit">Creat Page</button>
                </form>
            </div>
            <!--.Tab 1  Single product -->
            
 

    </div>
    <!--.Affiliate Accounts Start-->

    @endsection


    @section('menu')
<ul>
    <li>
        <a href="{{ url('pages') }}">Pages</a>
    </li>
    <li>
        <a href="{{ url('new') }}">Create Pages</a>

    </li>				<li>
        <a href="{{ url('users') }}">User Accounts</a>

    </li>
    <li>
        <a href="{{ url('affiliate') }}">Affiliate Accounts </a>

    </li>

</ul>
    @endsection
    @section('js')
    <!-- PLUGINS INITIALIZATION AND SETTINGS -->
    <!-- for IE support -->
    <script src="{{url('assets/globals/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{url('assets/add.js')}}"></script>
    <!-- END PLUGINS INITIALIZATION AND SETTINGS -->
    @endsection
    @section('init')
    <script>
var csrf = '{{ csrf_token() }}';
    </script>
    <!-- BEGIN INITIALIZATION-->
    <script>
        $(document).ready(function () {
            Pleasure.init();
            Layout.init();
        });
    </script>
    <!-- END INITIALIZATION-->
    @endsection
