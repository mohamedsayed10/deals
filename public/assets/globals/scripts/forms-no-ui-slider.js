var FormsNoUISlider = {

	basic: function () {
		$('.nouislider-basic').noUiSlider({
			start: [25, 75],
			connect: true,
			range: {
				'min': 0,
				'max': 100
			}
		});
	},

	range: function () {
		$('.nouislider-range').noUiSlider({
			start: [4000],
			range: {
				'min': 2000,
				'max': 10000
			}
		});
		$('.nouislider-range').Link('lower').to($('.nouislider-range-value'));
	},

	step: function () {
		$('.nouislider-step').noUiSlider({
			start: [5],
			step: 1,
			range: {
				'min': 5,
				'max': 30
			}
		});
		$('.nouislider-step').Link('lower').to($('.nouislider-step-value'));
	},

	rtl: function () {
		$('.nouislider-rtl').noUiSlider({
			start: 20,
			direction: "rtl",
			range: {
				'min': 0,
				'max': 100
			}
		});
		$('.nouislider-rtl').Link('lower').to($('.nouislider-rtl-value'));
	},

	init: function () {
		this.basic();
		this.range();
		this.step();
		this.handle();
		this.formatting();
		this.limits();
		this.rtl();
		this.toggle();
		this.vertical();
		this.date();
		this.pips();
		this.pipsRtl();
		this.pipsVertical();
		this.pipsRtlVertical();
		this.pipsValues();
		this.pipsPositions();

		this.setPipsRange(); // initilized for all Pips functions
	}
}

