/* for affiliate account page */
var TablesDataTablesEditor_affiliate_accounts = {
    restoreRow: function (oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }
        oTable.fnDraw();
    },
    editRow: function (oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[0] + '" class="form-control input-sm" disabled></div></div>';
        jqTds[1].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[1] + '" class="form-control input-sm"></div></div>';
        jqTds[2].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[2] + '" class="form-control input-sm"></div></div>';
        jqTds[3].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[3] + '" class="form-control input-sm"></div></div>';
        jqTds[4].innerHTML = '<a href="javascript:;" class="dt-edit">Save</a>';
        jqTds[5].innerHTML = '<a href="javascript:;" class="dt-cancel">Cancel</a>';
    },
    newRow: function (oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[0] + '" class="form-control input-sm" disabled></div></div>';
        jqTds[1].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[1] + '" class="form-control input-sm"></div></div>';
        jqTds[2].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[2] + '" class="form-control input-sm"></div></div>';
        jqTds[3].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[3] + '" class="form-control input-sm"></div></div>';
        jqTds[4].innerHTML = '<a href="javascript:;" class="dt-edit">Add</a>';
        jqTds[5].innerHTML = '<a href="javascript:;" class="dt-cancel">Cancel</a>';
    },
    saveRow: function (oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
        oTable.fnUpdate('<button class="btn btn-primary btn-xs dt-edit"><span class="glyphicon glyphicon-pencil"></span></button>', nRow, 4, false);
        oTable.fnUpdate('<button class="btn btn-danger btn-xs dt-delete"><span class="glyphicon glyphicon-trash"></span></button>', nRow, 5, false);
        oTable.fnDraw();
    }, newRowAdd: function (oTable, nRow, id) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(id, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
        oTable.fnUpdate('<button class="btn btn-primary btn-xs dt-edit"><span class="glyphicon glyphicon-pencil"></span></button>', nRow, 4, false);
        oTable.fnUpdate('<button class="btn btn-danger btn-xs dt-delete"><span class="glyphicon glyphicon-trash"></span></button>', nRow, 5, false);
        oTable.fnDraw();
    },
    fnGetSelected: function (oTable) {
        return oTable.$('tr.selected');
    },
    deleteButtonState: function (nSelected) {
        if (nSelected > 0)
            $('#dt-deleteall').prop('disabled', false)
        else {
            $('#dt-deleteall').prop('disabled', true);
            $('.checkable-group').prop('checked', false);
        }
    },
    editor: function () {
        var oTable = $('#afiiliate_accounts_table').dataTable({
            'columns': [{
                    'orderable': true
                }, {
                    'orderable': false
                },
                {
                    'orderable': true
                }, {
                    'orderable': true
                }, {
                    'orderable': false
                }, {
                    'orderable': false
                }],
            'order': [
                [1, "asc"]
            ],
            'dom': '<"toolbar pull-left">frtip'
        });
        $('div.toolbar').html('<button id="dt-new" class="btn btn-primary btn-sm margin-bottom-10">Add New Record</button>');
        var nEditing = null,
                nCreating = false,
                nSelected = 1;
        $('#dt-new').click(function (e) {
            e.preventDefault();
            var aiNew = oTable.fnAddData(['', '', '', '',
                '<button class="btn btn-primary btn-xs dt-edit"><span class="glyphicon glyphicon-pencil"></span></button>', '<button class="btn btn-danger btn-xs dt-delete"><span class="glyphicon glyphicon-trash"></span></button>']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            TablesDataTablesEditor_affiliate_accounts.newRow(oTable, nRow);
            nEditing = nRow;
            nCreating = true;
        });
        $('#dt-deleteall').click(function (e) {
            e.preventDefault();
            if (confirm("Are you sure to delete selected rows ?") == false) {
                return;
            }
            var anSelected = TablesDataTablesEditor_affiliate_accounts.fnGetSelected(oTable);
            for (var i = 0; i < anSelected.length; i++) {
                oTable.fnDeleteRow(anSelected[i]);
            }
            ;
            alert("Selected rows were deleted! Do not forget to do some ajax to sync with backend :)");
            $('.checkable-group').prop('checked', false);
            nSelected = 0;
        });
        oTable.on('click', '.dt-cancel', function (e) {
            e.stopPropagation();
            e.preventDefault();
            if (nCreating) {
                oTable.fnDeleteRow(nEditing);
                nCreating = false;
            } else {
                $(this).parents('tr').removeClass('selected');
                TablesDataTablesEditor_affiliate_accounts.restoreRow(oTable, nEditing);
                nEditing = null;
                nSelected--;
            }
        });
        oTable.on('click', '.dt-delete', function (e) {
            e.stopPropagation();
            e.preventDefault();
            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var request = $.ajax({
                url: '/DeleteAffiliate',
                type: 'POST',
                data: {
                    id: aData[0],
                    _token: csrf,
                },
                error: function () {
                    swal(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                            );
                },
                beforeSend: function () {

                },
                success: function (data) {
                    oTable.fnDeleteRow(nRow);
                    csrf = data.new_token;
                    swal(
                            'Good job!',
                            'Deleted! Successfully',
                            'success'
                            );
                }
            });
        });
        oTable.on('click', '.dt-edit', function (e) {
            e.stopPropagation();
            e.preventDefault();
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];
            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                TablesDataTablesEditor_affiliate_accounts.restoreRow(oTable, nEditing);
                TablesDataTablesEditor_affiliate_accounts.editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                TablesDataTablesEditor_affiliate_accounts.saveRow(oTable, nEditing);
                var aData = oTable.fnGetData(nRow);
                nEditing = null;
                var request = $.ajax({
                    url: '/UpdateAffiliate',
                    type: 'POST',
                    data: {
                        id: aData[0],
                        _token: csrf,
                        name: aData[1],
                        url: aData[2],
                        company: aData[3]
                    },
                    error: function () {
                        swal(
                                'Oops...',
                                'Something went wrong!',
                                'error'
                                );
                    },
                    beforeSend: function () {
                    },
                    success: function (data) {
                        csrf = data.new_token;
                        swal(
                                'Good job!',
                                'Updated! Successfully',
                                'success'
                                );
                    }
                });
            } else if (nEditing == nRow && this.innerHTML == "Add") {
                /* Editing this row and want to save it */
                var jqInputs = $('input', nRow);
                var request = $.ajax({
                    url: '/addAffiliate',
                    type: 'POST',
                    data: {
                        _token: csrf,
                        name: jqInputs[1].value,
                        url: jqInputs[2].value,
                        company: jqInputs[3].value
                    },
                    error: function () {
                        swal(
                                'Oops...',
                                'Something went wrong!',
                                'error'
                                );
                    },
                    beforeSend: function () {
                    },
                    success: function (data) {
                        TablesDataTablesEditor_affiliate_accounts.newRowAdd(oTable, nEditing, data.id);
                        nEditing = null;
                        csrf = data.new_token;
                        swal(
                                'Good job!',
                                'added! Successfully',
                                'success'
                                );
                    }

                });
            } else {
                /* No edit in progress - let's start one */
                TablesDataTablesEditor_affiliate_accounts.editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    },
    init: function () {
        this.editor();
        $.extend($.fn.dataTable.defaults, {
            fnDrawCallback: function (oSettings) {
                $('.dataTables_wrapper select, .dataTables_wrapper input').removeClass('input-sm');
            }
        });
    }
}


/* for affiliate account page */
var TablesDataTablesEditor_users_accounts = {
    restoreRow: function (oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }
        oTable.fnDraw();
    },
    editRow: function (oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[0] + '" class="form-control input-sm" disabled></div></div>';
        jqTds[1].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[1] + '" class="form-control input-sm"></div></div>';
        jqTds[2].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[2] + '" class="form-control input-sm"></div></div>';
        jqTds[3].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="password" value="' + aData[3] + '" class="form-control input-sm"></div></div>';
        jqTds[4].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="password" value="' + aData[4] + '" class="form-control input-sm"></div></div>';
        jqTds[5].innerHTML = '<a href="javascript:;" class="dt-edit">Save</a>';
        jqTds[6].innerHTML = '<a href="javascript:;" class="dt-cancel">Cancel</a>';
    },
    newRow: function (oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[0] + '" class="form-control input-sm" disabled></div></div>';
        jqTds[1].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[1] + '" class="form-control input-sm"></div></div>';
        jqTds[2].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[2] + '" class="form-control input-sm"></div></div>';
        jqTds[3].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="password" value="' + aData[3] + '" class="form-control input-sm"></div></div>';
        jqTds[4].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="password" value="' + aData[4] + '" class="form-control input-sm"></div></div>';
        jqTds[5].innerHTML = '<a href="javascript:;" class="dt-edit">Add</a>';
        jqTds[6].innerHTML = '<a href="javascript:;" class="dt-cancel">Cancel</a>';
    },
    saveRow: function (oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate('******', nRow, 3, false);
        oTable.fnUpdate('******', nRow, 4, false);
        oTable.fnUpdate('<button class="btn btn-primary btn-xs dt-edit"><span class="glyphicon glyphicon-pencil"></span></button>', nRow, 5, false);
        oTable.fnUpdate('<button class="btn btn-danger btn-xs dt-delete"><span class="glyphicon glyphicon-trash"></span></button>', nRow, 6, false);
        oTable.fnDraw();
    }, newRowAdd: function (oTable, nRow, id) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(id, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate('******', nRow, 3, false);
        oTable.fnUpdate('******', nRow, 4, false);
        oTable.fnUpdate('<button class="btn btn-primary btn-xs dt-edit"><span class="glyphicon glyphicon-pencil"></span></button>', nRow, 5, false);
        oTable.fnUpdate('<button class="btn btn-danger btn-xs dt-delete"><span class="glyphicon glyphicon-trash"></span></button>', nRow, 6, false);
        oTable.fnDraw();
    },
    fnGetSelected: function (oTable) {
        return oTable.$('tr.selected');
    },
    deleteButtonState: function (nSelected) {
        if (nSelected > 0)
            $('#dt-deleteall').prop('disabled', false)
        else {
            $('#dt-deleteall').prop('disabled', true);
            $('.checkable-group').prop('checked', false);
        }
    },
    editor: function () {
        var oTable = $('#users_acounts_table').dataTable({
            'columns': [{
                    'orderable': true
                }, {
                    'orderable': true
                },
                {
                    'orderable': true
                },
                {
                    'orderable': false
                }, {
                    'orderable': false
                }, {
                    'orderable': false
                }, {
                    'orderable': false
                }],
            'order': [
                [1, "asc"]
            ],
            'dom': '<"toolbar2 pull-left">frtip'
        });
        $('div.toolbar2').html('<button id="dt-new2" class="btn btn-primary btn-sm margin-bottom-10">Add New Record</button>');
        var nEditing = null,
                nCreating = false,
                nSelected = 0;
        $('#dt-new2').click(function (e) {
            e.preventDefault();
            var aiNew = oTable.fnAddData(['', '', '', '', '',
                '<button class="btn btn-primary btn-xs dt-edit"><span class="glyphicon glyphicon-pencil"></span></button>', '<button class="btn btn-danger btn-xs dt-delete"><span class="glyphicon glyphicon-trash"></span></button>']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            TablesDataTablesEditor_users_accounts.newRow(oTable, nRow);
            nEditing = nRow;
            nCreating = true;
        });
        oTable.on('click', '.dt-cancel', function (e) {
            e.stopPropagation();
            e.preventDefault();
            if (nCreating) {
                oTable.fnDeleteRow(nEditing);
                nCreating = false;
            } else {
                $(this).parents('tr').removeClass('selected');
                TablesDataTablesEditor_users_accounts.restoreRow(oTable, nEditing);
                nEditing = null;
                nSelected--;
            }
        });
        oTable.on('click', '.dt-delete', function (e) {
            e.stopPropagation();
            e.preventDefault();
            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var request = $.ajax({
                url: '/DeleteUser',
                type: 'POST',
                data: {
                    id: aData[0],
                    _token: csrf
                },
                error: function () {
                    swal(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                            );
                },
                beforeSend: function () {

                },
                success: function (data) {
                    oTable.fnDeleteRow(nRow);
                    csrf = data.new_token;
                    swal(
                            'Good job!',
                            'Deleted! Successfully',
                            'success'
                            );
                }
            });
        });
        oTable.on('click', '.dt-edit', function (e) {
            e.stopPropagation();
            e.preventDefault();
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];
            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                TablesDataTablesEditor_users_accounts.restoreRow(oTable, nEditing);
                TablesDataTablesEditor_users_accounts.editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                var jqInputs = $('input', nRow);
                var request = $.ajax({
                    url: '/UpdateUser',
                    type: 'POST',
                    data: {
                        id: jqInputs[0].value,
                        _token: csrf,
                        name: jqInputs[1].value,
                        email: jqInputs[2].value,
                        password: jqInputs[3].value,
                        password_confirmation: jqInputs[4].value
                    },
                    error: function () {
                        swal(
                                'Oops...',
                                'Something went wrong!',
                                'error'
                                );
                    },
                    beforeSend: function () {
                    },
                    success: function (data) {
                        TablesDataTablesEditor_users_accounts.saveRow(oTable, nEditing, data.id);
                        nEditing = null;
                        csrf = data.new_token;
                        swal(
                                'Good job!',
                                'Updated! Successfully',
                                'success'
                                );
                    }
                });
            } else if (nEditing == nRow && this.innerHTML == "Add") {
                /* Editing this row and want to save it */
                var jqInputs = $('input', nRow);
                var request = $.ajax({
                    url: '/AddUser',
                    type: 'POST',
                    data: {
                        _token: csrf,
                        name: jqInputs[1].value,
                        email: jqInputs[2].value,
                        password: jqInputs[3].value,
                        password_confirmation: jqInputs[4].value

                    },
                    error: function () {
                        swal(
                                'Oops...',
                                'Something went wrong!',
                                'error'
                                );
                    },
                    beforeSend: function () {
                    },
                    success: function (data) {
                        TablesDataTablesEditor_users_accounts.newRowAdd(oTable, nEditing, data.id);
                        nEditing = null;
                        csrf = data.new_token;
                        swal(
                                'Good job!',
                                'added! Successfully',
                                'success'
                                );
                    }
                });
            } else {
                /* No edit in progress - let's start one */
                TablesDataTablesEditor_users_accounts.editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    },
    init: function () {
        this.editor();
        $.extend($.fn.dataTable.defaults, {
            fnDrawCallback: function (oSettings) {
                $('.dataTables_wrapper select, .dataTables_wrapper input').removeClass('input-sm');
            }
        });
    }
}

var TablesDataTablesEditor_pages_view = {
    restoreRow: function (oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
            oTable.fnUpdate(aData[i], nRow, i, false);
        }
        oTable.fnDraw();
    },
    editRow: function (oTable, nRow) {
        var aData = oTable.fnGetData(nRow);
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[0] + '" class="form-control input-sm"></div></div>';
        jqTds[1].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[1] + '" class="form-control input-sm"></div></div>';
        jqTds[2].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[2] + '" class="form-control input-sm"></div></div>';
        jqTds[3].innerHTML = '<div class="inputer"><div class="input-wrapper"><input type="text" value="' + aData[3] + '" class="form-control input-sm"></div></div>';
        jqTds[4].innerHTML = '<a href="javascript:;" class="dt-edit">Save</a>  <a href="javascript:;" class="dt-cancel">Cancel</a>';
    },
    saveRow: function (oTable, nRow) {
        var jqInputs = $('input', nRow);
        oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
        oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
        oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
        oTable.fnUpdate('<button class="btn btn-primary btn-xs dt-edit"><span class="glyphicon glyphicon-pencil"></span></button><button class="btn btn-info btn-xs dt-delete btn-ripple"><span class="glyphicon glyphicon-eye-open"></span></button>', nRow, 4, false);
        oTable.fnDraw();
    },
    fnGetSelected: function (oTable) {
        return oTable.$('tr.selected');
    },
    deleteButtonState: function (nSelected) {
        if (nSelected > 0)
            $('#dt-deleteall').prop('disabled', false)
        else {
            $('#dt-deleteall').prop('disabled', true);
            $('.checkable-group').prop('checked', false);
        }
    },
    editor: function () {
        var oTable = $('#pages_view').dataTable({
            'columns': [{
                    'orderable': true
                }, {
                    'orderable': true
                }, {
                    'orderable': true
                }, {
                    'orderable': true
                }, {
                    'orderable': true
                }, {
                    'orderable': false
                }],
            'order': [
                [1, "asc"]
            ],
            'dom': '<"toolbar pull-left">frtip'
        });
        $('#dt-new').click(function (e) {
            e.preventDefault();
            var aiNew = oTable.fnAddData(['', '', '', '',
                '<button class="btn btn-primary btn-xs dt-edit"><span class="glyphicon glyphicon-pencil"></span></button>', '<button class="btn btn-danger btn-xs dt-delete"><span class="glyphicon glyphicon-trash"></span></button>']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            TablesDataTablesEditor_affiliate_accounts.editRow(oTable, nRow);
            nEditing = nRow;
            nCreating = true;
        });
        $('#dt-deleteall').click(function (e) {
            e.preventDefault();
            if (confirm("Are you sure to delete selected rows ?") == false) {
                return;
            }
            var anSelected = TablesDataTablesEditor_affiliate_accounts.fnGetSelected(oTable);
            for (var i = 0; i < anSelected.length; i++) {
                oTable.fnDeleteRow(anSelected[i]);
            }
            ;
            alert("Selected rows were deleted! Do not forget to do some ajax to sync with backend :)");
            $('.checkable-group').prop('checked', false);
            nSelected = 0;
        });
        oTable.on('click', '.dt-cancel', function (e) {
            e.stopPropagation();
            e.preventDefault();
            if (nCreating) {
                oTable.fnDeleteRow(nEditing);
                nCreating = false;
            } else {
                $(this).parents('tr').removeClass('selected');
                TablesDataTablesEditor_affiliate_accounts.restoreRow(oTable, nEditing);
                nEditing = null;
                nSelected--;
            }
        });
        oTable.on('click', '.dt-delete', function (e) {
            e.stopPropagation();
            e.preventDefault();
            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);

            var regex = /(multiple|single)/g;
            var str = aData[3];
            var m;
            while ((m = regex.exec(str)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
            }
            var res = str.match(regex);
            if (res[0] === 'multiple') {
                var multiple = $.ajax({
                    url: '/DeleteMultiple',
                    type: 'POST',
                    data: {
                        id: aData[0],
                        _token: csrf
                    },
                    error: function () {
                        swal(
                                'Oops...',
                                'Something went wrong!',
                                'error'
                                );
                    },
                    beforeSend: function () {

                    },
                    success: function (data) {
                        oTable.fnDeleteRow(nRow);
                        csrf = data.new_token;
                        swal(
                                'Good job!',
                                'Deleted! Multiple',
                                'success'
                                );
                        ;
                    }
                });
            }
            else if (res[0] === 'single') {
                var Single = $.ajax({
                    url: '/DeleteSingle',
                    type: 'POST',
                    data: {
                        id: aData[0],
                        _token: csrf
                    },
                    error: function () {
                        swal(
                                'Oops...',
                                'Something went wrong!',
                                'error'
                                );
                    },
                    beforeSend: function () {

                    },
                    success: function (data) {
                        oTable.fnDeleteRow(nRow);
                        csrf = data.new_token;
                        swal(
                                'Good job!',
                                'Deleted! Single',
                                'success'
                                );
                    }
                });
            }
            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
        }
        );
        oTable.on('click', '.dt-edit', function (e) {
            e.stopPropagation();
            e.preventDefault();
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];
            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                TablesDataTablesEditor_affiliate_accounts.restoreRow(oTable, nEditing);
                TablesDataTablesEditor_affiliate_accounts.editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                TablesDataTablesEditor_affiliate_accounts.saveRow(oTable, nEditing);
                nEditing = null;
                alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                TablesDataTablesEditor_affiliate_accounts.editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    },
    init: function () {
        this.editor();
        $.extend($.fn.dataTable.defaults, {
            fnDrawCallback: function (oSettings) {
                $('.dataTables_wrapper select, .dataTables_wrapper input').removeClass('input-sm');
            }
        });
    }
}
