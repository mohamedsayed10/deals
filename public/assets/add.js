$(document).ready(function () {
    $('#single').submit(function (e) {
        e.preventDefault();
        var request = $.ajax({
            url: '/single',
            type: 'POST',
            data: {
                _token: csrf,
                single_title: $('#single_title').val(),
                single_description: $('#single_description').val(),
                single_url: $('#single_url').val(),
                single_affiliate: $('#single_affiliate').val(),
                single_company: $('#single_company').val()
            },
            error: function () {
                swal(
                        'Oops...',
                        'Something went wrong!',
                        'error'
                        );
            },
            beforeSend: function () {
            },
            success: function (data) {
                csrf = data.new_token;
                swal(
                        'Good job!',
                        'added! Successfully',
                        'success'
                        );
            }
        });
    });

    $('#single_update').submit(function (e) {
        e.preventDefault();
        var request = $.ajax({
            url: '/UpdateSingle',
            type: 'POST',
            data: {
                id: $('#single_id').val(),
                _token: csrf,
                single_title: $('#single_title').val(),
                single_description: $('#single_description').val(),
                single_url: $('#single_url').val(),
                single_affiliate: $('#single_affiliate').val(),
                single_company: $('#single_company').val()
            },
            error: function () {
                swal(
                        'Oops...',
                        'Something went wrong!',
                        'error'
                        );
            },
            beforeSend: function () {
            },
            success: function (data) {
                csrf = data.new_token;
                swal(
                        'Good job!',
                        'added! Successfully',
                        'success'
                        );
            }
        });
    });
    $('#multi').submit(function (e) {
        e.preventDefault();
        var urls = [];
        var sliders = [];
        var affiliates = [];
        $('.urls').each(function (index) {
            urls[index] = $("input.urls" + index).val();
        });
        $('.multi_affiliates').each(function (index) {
            affiliates[index] = $("select.multi_affiliates" + index).val();
        });
        $('.sliders').each(function (index) {
            sliders[index] = $("select.sliders" + index).val();
        });
        var request = $.ajax({
            url: '/multiple',
            type: 'POST',
            data: {
                _token: csrf,
                multi_title: $('#multi_title').val(),
                multi_description: $('#multi_description').val(),
                urls: urls,
                sliders: sliders,
                affiliates: affiliates
            },
            error: function () {
                swal(
                        'Oops...',
                        'Something went wrong!',
                        'error'
                        );
            },
            beforeSend: function () {
            },
            success: function (data) {
                csrf = data.new_token;
                swal(
                        'Good job!',
                        'added! Successfully',
                        'success'
                        );
            }
        });
    });
    $('#multi_update').submit(function (e) {
        e.preventDefault();
        var urls = [];
        var sliders = [];
        var affiliates = [];
        $('.urls').each(function (index) {
            urls[index] = $("input.urls" + index).val();
        });
        $('.multi_affiliates').each(function (index) {
            affiliates[index] = $("select.multi_affiliates" + index).val();
        });
        $('.sliders').each(function (index) {
            sliders[index] = $("select.sliders" + index).val();
        });
        console.log($('#multi_title').val());
        var request = $.ajax({
            url: '/UpdateMultiple',
            type: 'POST',
            data: {
                id: $('#page_id').val(),
                _token: csrf,
                multi_title: $('#multi_title').val(),
                multi_description: $('#multi_description').val(),
                urls: urls,
                sliders: sliders,
                affiliates: affiliates
            },
            error: function () {
                swal(
                        'Oops...',
                        'Something went wrong!',
                        'error'
                        );
            },
            beforeSend: function () {
            },
            success: function (data) {
                csrf = data.new_token;
                swal(
                        'Good job!',
                        'added! Successfully',
                        'success'
                        );
            }
        });
    });
    $('.ssss-btn').click(function () {
        var regex = /(multiple|single)/g;
        var str = $(this).parent('td').prevAll('.type').text();
        var id = $(this).parent('td').prevAll('.item_id').text();
        var m;
        while ((m = regex.exec(str)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
        }
        var res = str.match(regex);
        if (res[0] === 'multiple') {
            var link = "/ShowMultiple/" + id;
            console.log(link);
            var multiple = $.ajax({
                url: link,
                type: 'GET',
                error: function () {
                    swal(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                            );
                },
                beforeSend: function () {
                    swal({
                        title: 'Auto close alert!',
                        text: 'I will close in 2 seconds.',
                        timer: 2000
                    })
                },
                success: function (data) {
                    var values = data.data;
                    var limits = values.limits.split(',');
                    var links = values.links.split(',');
                    var affiliates = values.affiliates.split(',');
                    var linkss;
                    csrf = data.new_token;
                }
            });
        }
        else if (res[0] === 'single') {

            var Single = $.ajax({
                url: '/UpdateSingle',
                type: 'GET',
                data: {
                    id: id,
                },
                error: function () {
                    swal(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                            );
                },
                beforeSend: function () {
                },
                success: function (data) {
                    csrf = data.new_token;
                    swal(
                            'Good job!',
                            'added! Successfully',
                            'success'
                            );
                }
            });
        }




    });
});