<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultiViews extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'multi_views';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the post that owns the comment.
     */
    public function multiple() {
        return $this->belongsTo('App\MultiplePage');
    }

}
