<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SinglePage extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'single_pages';

    /**
     * Get the comments for the blog post.
     */
    public function Unique_Views() {
        return $this->belongsTo('App\singleViews', 'id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function Affiliate() {
        return $this->belongsTo('App\Affiliate', 'affiliate');
    }

}
