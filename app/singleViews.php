<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class singleViews extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'single_views';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the post that owns the comment.
     */
    public function Single() {
        return $this->hasMany('App\SinglePage','page_id')->count();
    }

}
