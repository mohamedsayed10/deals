<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultiplePage extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'multiple_pages';

    /**
     * Get the comments for the blog post.
     */
    public function unique_views() {
        return $this->hasMany('App\MultiViews', 'page_id');
    }

}
