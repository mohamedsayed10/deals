<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'affiliates';

    /**
     * Get the post that owns the comment.
     */
    public function Single() {
        
        return $this->hasOne('App\SinglePage','id');
    }

}
