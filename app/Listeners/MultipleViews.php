<?php

namespace App\Listeners;

use App\Events\MultipleWasViewed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MultiplePage;
use App\MultiViews;
use Request;

class MultipleViews {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MultipleWasViewed  $event
     * @return void
     */
    public function handle(MultipleWasViewed $event) {
        //counting views
        $views = MultiplePage::find($event->multi);
        $views->views += 1;
        $views->save();

        $ip = Request::ip();
        //checking if it viewed before by the visitor
        $uviews = MultiViews::where('page_id', $event->multi)
                ->where('visitor', $ip)
                ->count();
        if ($uviews) {
            return;
        } else {
            //counting unique views
            $view = new MultiViews;
            $view->page_id = $event->multi;
            $view->visitor = $ip;
            $view->save();
        }
    }

}
