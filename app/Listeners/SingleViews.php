<?php

namespace App\Listeners;

use Request;
use App\singleViews as SingleView;
use App\SinglePage;
use App\Events\SingleWasViewed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SingleViews {

    /**
     * Handle the event.
     *
     * @param  MultipleWasViewed  $event
     * @return void
     */
    public function handle(SingleWasViewed $event) {
        //counting views

        $views = SinglePage::find($event->single);
        $views->views += 1;
        $views->save();


        $ip = Request::ip();

        $uviews = SingleView::where('page_id', $event->single)
                ->where('visitor', $ip)
                ->count();
        //checking if it viewed before by the visitor

        if ($uviews) {
            return;
        } else {
            //counting unique views

            $view = new SingleView;
            $view->page_id = $event->single;
            $view->visitor = $ip;
            $view->save();
        }
    }

}
