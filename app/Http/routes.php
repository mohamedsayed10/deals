<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
  <<<<<<< HEAD
 */

Route::get('/{id}-{title}', 'ViewController@showMultiple');
Route::get('/single/{id}-{title}', 'ViewController@showSingle');
// Authentication routes...
Route::get('/login', 'Auth\AuthController@getLogin');
Route::post('/login', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/pages', 'PageController@showPages');
    Route::get('/affiliate', 'AffiliateController@index');
    Route::get('/new', 'PageController@CreateNew');
    Route::post('/multiple', 'PageController@multiple');
    Route::post('/DeleteMultiple', 'PageController@DeleteMultiple');
    Route::post('/DeleteSingle', 'PageController@DeleteSingle');
    Route::post('/single', 'PageController@single');
    Route::post('/UpdateMultiple', 'PageController@UpdateMultiple');
    Route::post('/UpdateSingle', 'PageController@UpdateSingle');
    Route::get('/users', 'UserController@showUser');
    Route::get('/ShowMultiple/{id}', 'PageController@ShowMultiple');
    Route::get('/ShowSingle/{id}', 'PageController@ShowSingle');
    Route::post('/addAffiliate', 'AffiliateController@addAffiliate');
    Route::post('/UpdateAffiliate', 'AffiliateController@UpdateAffiliate');
    Route::post('/DeleteAffiliate', 'AffiliateController@DeleteAffiliate');
    Route::get('/register', 'PageController@showRegister');
    Route::post('/AddUser', 'UserController@AddUser');
    Route::post('/DeleteUser', 'UserController@DeleteUser');
    Route::post('/UpdateUser', 'UserController@UpdateUser');
});
Route::get('/home', 'HomeController@index');
