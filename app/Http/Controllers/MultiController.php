<?php

namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use Symfony\Component\DomCrawler\Crawler;

class MultiController extends Controller {

    /**
     * getting data from site.
     * @param $url
     * @return \Illuminate\Http\Response
     */
    private static function getData($url) {
        //sending Get request to $url
        //using cURL library url https://github.com/anlutro/php-curl
        $request = cURL::newRequest('get', $url)
                ->setHeader('Accept-Charset', 'utf-8')
                ->setHeader('Accept-Language', 'en-US')
                ->setOption(CURLOPT_FOLLOWLOCATION, true)
                ->setOption(CURLOPT_AUTOREFERER, true)
                ->setOption(CURLOPT_REFERER, 'http://www.google.com')
                ->setOption(CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0')
                ->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $response = $request->send();
        return $response;
    }

    /**
     * getting data from site.
     * @param $url
     * @return \Illuminate\Http\Response
     */
    public static function namshi($url, $limit = 4) {

        $response = self::getData($url);
        //getting the url of parent link
        $re = "@^(?:(https|http)://)?([^/]+)@i";
        preg_match($re, $url, $matches);
        $parentLink = $matches[0];
        //using Symfony Crawler
        $crawler = new Crawler($response->body);
        //filtering and getting data
        $items = $crawler->filter('.listing > a')->each(function (Crawler $node, $i) use ($parentLink, $limit) {
            //getting title
            $title = $node->attr('title');
            //getting image
            $image = $node->filter('.image_container > img')->attr('data-src');
            //getting link
            $link = $parentLink . $node->attr('href');
            //getting brand
            $brand = $node->filter('.details_container > .brand')->text();
            //getting desciption
            $descrption = $node->filter('.details_container > .description')->text();
            //getting price
            $price = $node->filter('.details_container > .price')->text();
            $product = ['title' => $title, 'image' => $image, 'link' => $link,
                'brand' => $brand, 'descrption' => $descrption, 'price' => $price];
            if ($i < $limit) {
                return $product;
            }
        });
        return $items;
    }

    /**
     * getting data from site.
     * @param $url
     * @return \Illuminate\Http\Response
     */
    public static function souq($url, $limit = 4) {
        $response = self::getData($url);
        $crawler = new Crawler($response->body);
        //using Symfony Crawler
        $items = $crawler->filter('li > .placard')->each(function (Crawler $node, $i) use ($limit) {
            //getting title
            $title = $node->filter('h6 > a')->text();
            //getting image
            $image = $node->filter('a > img')->attr('src');
            //getting link
            $link = $node->filter('h6 > a')->attr('href');
            //getting price
            $price = $node->filter('.price > .is')->text();
            $wasPrice = $node->filter('.price > .was')->text();
            $product = ['title' => $title, 'image' => $image, 'link' => $link,
                'price' => $price, 'wasPrice' => $wasPrice];
            if ($i < $limit) {
                return $product;
            }
        });

        return $items;
    }

    /**
     * getting data from site.
     * @param $url
     * @return \Illuminate\Http\Response
     */
    public static function jumia($url, $limit = 4) {
        $response = self::getData($url);
        //using Symfony Crawler
        $crawler = new Crawler($response->body);
        $items = $crawler->filter('.products > div')->each(function (Crawler $node, $i) use ($limit) {
            //getting title
            $title = $node->filter('.name')->text();
            //getting image
            $image = $node->filter('.image')->attr('data-src');
            //getting link
            $link = $node->filter('a')->attr('href');
            //getting price
            $price = $node->filter('.price')->text();
            $wasPrice = $node->filter('.price')->text();
            $product = ['title' => $title, 'image' => $image, 'link' => $link,
                'price' => $price, 'wasPrice' => $wasPrice];
            if ($i < $limit) {
                return $product;
            }
        });

        return $items;
    }

}
