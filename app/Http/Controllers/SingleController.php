<?php

namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use Symfony\Component\DomCrawler\Crawler;

class SingleController extends Controller
{

    /**
     * getting data from site.
     * @param $url
     * @return \Illuminate\Http\Response
     */
    private static function getData($url)
    {
        //sending Get request to $url
        //using cURL library url https://github.com/anlutro/php-curl
        $request = cURL::newRequest('get', $url)
            ->setHeader('Accept-Charset', 'utf-8')
            ->setHeader('Accept-Language', 'en-US')
            ->setOption(CURLOPT_FOLLOWLOCATION, true)
            ->setOption(CURLOPT_AUTOREFERER, true)
            ->setOption(CURLOPT_REFERER, 'http://www.google.com')
            ->setOption(CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0')
            ->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $response = $request->send();
        return $response;
    }

    /**
     * getting data from site.
     * @param $url
     * @return \Illuminate\Http\Response
     */
    public static function namshi($url)
    {
        $response = self::getData($url);
        //using Symfony Crawler
        $crawler = new Crawler($response->body);
        //getting title
        $title = $crawler->filter('title')->text();
        //getting description
        $descrption = $crawler->filter('.info_content')->html();
        //getting link
        $link = $url;
        //getting images
        $images = $crawler->filter('#product_carousel > ul > li > a > img')->each(function (Crawler $node) {
            return $node->attr('src');
        });
        //getting price
        $price   = $crawler->filter('.price')->text();
        $product = ['title' => $title, 'images'     => $images, 'link' => $link,
            'descrption'        => $descrption, 'price' => $price];
        return $product;
    }

    /**
     * getting data from site.
     * @param $url
     * @return \Illuminate\Http\Response
     */
    public static function souq($url)
    {
        $response = self::getData($url);
        $crawler  = new Crawler($response->body);
        //getting title
        $title = $crawler->filter('h1[itemprop="name"]')->text();
        //getting descrption
        $descrption = $crawler->filter('#description > article')->html();

        //getting url
        $link = $url;
        //getting images
        $images = $crawler->filter('.img-bucket > img')->each(function (Crawler $node) {
            return $node->attr('src');
        });
        //getting price
        $price   = $crawler->filter('.price')->text();
        $product = ['title' => $title, 'images'     => $images, 'link' => $link,
            'descrption'        => $descrption, 'price' => $price];
        return $product;
    }

    /**
     * getting data from site.
     * @param $url
     * @return \Illuminate\Http\Response
     */
    public static function jumia($url)
    {
        $response = self::getData($url);
        $crawler  = new Crawler($response->body);
        //getting title
        $title = $crawler->filter('.details-wrapper > div > span')->text();
        //getting description
        $descrption = $crawler->filter('.product-description')->html();
        //getting link
        $link = $url;
        //getting images
        $images = $crawler->filter('.product-preview > img')->each(function (Crawler $node) {
            return $node->attr('data-src');
        });
        //getting price
        $price   = $crawler->filter('.price')->text();
        $product = ['title' => $title, 'images'     => $images, 'link' => $link,
            'descrption'        => $descrption, 'price' => $price];
        return $product;
    }

}
