<?php

namespace App\Http\Controllers;

use App\MultiplePage;
use App\SinglePage;
use App\Affiliate;
use App\template;
use App\Events\MultipleWasViewed;
use App\Events\SingleWasViewed;

class ViewController extends Controller {

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  string  $title
     * @return \Illuminate\Http\Response
     */
    public function showMultiple($id, $title) {
        $Pages = new MultiplePage;
        $multi = $Pages::where('id', $id)
                ->where('title', $title)
                ->firstOrFail();
        $affiliates = explode(',', $multi->affiliate_id);
        $NumAffiliate =[];
        foreach($affiliates as $key => $affiliate){
            $NumAffiliate[$key] = Affiliate::findOrFail($affiliate);
        }
        //Firing The views event
        event(new MultipleWasViewed($id));
        $links = explode(',', $multi->links);
        $limit = explode(',', $multi->limits);
        $PageTitle = str_replace("-", " ", $multi->title);
        $PageDescription = $multi->description;
        $exLink = [];
        foreach ($links as $key => $link) {
            $re = "/souq|namshi|jumia/";
            preg_match($re, $link, $matches);
            $company = $matches[0];
            switch ($company) {
                case 'souq':
                    $items = MultiController::souq($link, $limit[$key]);
                    break;
                case 'jumia':
                    $items = MultiController::jumia($link, $limit[$key]);
                    break;
                case 'namshi':
                    $items = MultiController::namshi($link, $limit[$key]);

                    break;
            }
            $exLink[$key] = ['products' => $items, 'company' => $matches[0]];
        }
        $template = template::findOrFail($multi->template_id);
        return view($template->route, compact('exLink', 'NumAffiliate', 'PageTitle', 'PageDescription'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  string  $title
     * @return \Illuminate\Http\Response
     */
    public function showSingle($id, $title) {
        $Page = new SinglePage;
        $single = $Page::where('id', $id)
                ->where('title', $title)
                ->firstOrFail();
        //Firing The normal views event
        event(new SingleWasViewed($id));

        //getting company
        $company = $single->company;
        switch ($company):
            case "souq":
                $item = SingleController::souq($single->url);
                break;
            case "jumia":
                $item = SingleController::jumia($single->url);
                break;
            case "namshi":
                $item = SingleController::namshi($single->url);
                break;
        endswitch;

        return view('product', compact('item'));
    }

}
