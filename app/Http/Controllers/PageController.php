<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\MultiplePage;
use App\SinglePage;
use App\Affiliate;
use App\template;
use App\singleViews;

class PageController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showPages() {
        $multiples = MultiplePage::all();
        $multiple_items = [];

        foreach ($multiples as $key => $multiple) {
            $views = MultiplePage::find($multiple->id)->unique_views()->count();
            $affiliates = explode(',', $multiple->affiliate_id);
            $urls = explode(',', $multiple->links);
            $url = [];
            $aff = [];
            foreach ($affiliates as $key2 => $affiliate) {
                $aff[$key2] = Affiliate::findOrFail($affiliate);
                $url[$key2] = $urls[$key2];
            }
            $multiple_items[$key] = ['affiliates' => $aff, 'item' => ['item' => $multiple, 'views' => $views], 'urls' => $url];
        }
        $SinglePages = SinglePage::all();

        $SinglePages->load('Affiliate');
        $single_pages = [];
        foreach ($SinglePages as $key => $single_page) {
            $single_views = singleViews::where('page_id', $single_page->id)->count();
            $single_pages[$key] = ['item' => $single_page, 'views' => $single_views];
        }

        return view('pages', compact('multiple_items', 'single_pages'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateNew() {
        $affiliates = Affiliate::all();
        $template = template::firstOrFail();
        return view('new', compact('affiliates', 'template'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowMultiple($id) {
        $multiple_page = MultiplePage::findOrFail($id);
        $links = explode(',', $multiple_page->links);
        $limit = explode(',', $multiple_page->limits);
        $affiliate_arr = explode(',', $multiple_page->affiliate_id);
        $affiliates = Affiliate::all();
        return view('update', compact('affiliates', 'multiple_page', 'links', 'limit', 'affiliate_arr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function multiple(Request $request) {
        $validator = Validator::make($request->all(), [
                    'multi_title' => 'required|string|min:6',
                    'multi_description' => 'required|string|min:6',
                    'urls.*' => 'required|url',
                    'sliders.*' => 'required|numeric|min:1',
                    'affiliates.*' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            return Response()->json(['error' => true], 403);
        }

        $title = str_replace(" ", "-", $request->multi_title);
        $affiliates = implode(',', $request->affiliates);
        $limits = implode(',', $request->sliders);
        $links = implode(',', $request->urls);

        //Saving the multiple page
        $Pages = new MultiplePage;
        $Pages->title = $title;
        $Pages->description = $request->multi_description;
        $Pages->links = $links;
        $Pages->template_id = 1;
        $Pages->affiliate_id = $affiliates;
        $Pages->limits = $limits;
        $Pages->save();
        return Response()->json(['new_token' => csrf_token()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdateMultiple(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required|numeric',
                    'multi_title' => 'required|string|min:6',
                    'multi_description' => 'required|string|min:6',
                    'urls.*' => 'required|url',
                    'limits.*' => 'required|numeric|min:1',
                    'affiliates.*' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            if ($validator->fails()) {
                return Response()->json(['error' => true], 403);
            }
        }
        $title = str_replace(" ", "-", $request->multi_title);
        //Saving the multiple page
        $Pages = MultiplePage::find($request->id);
        $links = implode(',', $request->urls);
        $limits = implode(',', $request->sliders);
        $affiliates = implode(',', $request->affiliates);
        $Pages->title = $title;
        $Pages->description = $request->multi_description;
        $Pages->links = $links;
        $Pages->template_id = 1;
        $Pages->affiliate_id = $affiliates;
        $Pages->limits = $limits;
        $Pages->save();
        return Response()->json(['new_token' => csrf_token()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function DeleteMultiple(Request $request) {
        $Pages = MultiplePage::find($request->id);
        $Pages->delete();
        return Response()->json(['new_token' => csrf_token()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function DeleteSingle(Request $request) {
        $Single = MultiplePage::find($request->id);
        $Single->delete();
        return Response()->json(['new_token' => csrf_token()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function single(Request $request) {
        $validator = Validator::make($request->all(), [
                    'single_title' => 'required|string|min:6',
                    'single_description' => 'required|string|min:6',
                    'single_affiliate' => 'required|numeric|min:0',
                    'single_url' => 'required|url',
                    'single_company' => 'required|string',
        ]);
        if ($validator->fails()) {
            return Response()->json(['error' => true], 403);
        }
        $title = str_replace(" ", "-", $request->single_title);

        //Saving the single page
        $Pages = new SinglePage;
        $Pages->title = $title;
        $Pages->description = $request->single_description;
        $Pages->affiliate = $request->single_affiliate;
        $Pages->company = $request->single_company;
        $Pages->url = $request->single_url;
        $Pages->save();
        return Response()->json(['new_token' => csrf_token()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function ShowSingle($id) {
        $single_page = SinglePage::findOrFail($id);
        $affiliates = Affiliate::all();
        return view('single', compact('single_page', 'affiliates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdateSingle(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required|numeric',
                    'single_title' => 'required|string|min:6',
                    'single_description' => 'required|string|min:6',
                    'single_affiliate' => 'required|numeric|min:0',
                    'single_url' => 'required|url',
                    'single_company' => 'required|string',
        ]);
        if ($validator->fails()) {
            return Response()->json(['error' => true], 403);
        }
        $title = str_replace(" ", "-", $request->single_title);
        //Saving the single page
        $Pages = SinglePage::find($request->id);
        $Pages->title = $title;
        $Pages->description = $request->single_description;
        $Pages->affiliate = $request->single_affiliate;
        $Pages->company = $request->single_company;
        $Pages->url = $request->single_url;
        $Pages->save();
        return Response()->json(['new_token' => csrf_token()], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
