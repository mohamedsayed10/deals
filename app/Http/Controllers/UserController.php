<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Hash;
use App\User;
use Illuminate\Http\Response;
use Validator;

class UserController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showUser() {
        $users = User::all();
        return view('users', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function AddUser(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return Response()->json(['error' => true], 403);
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        return Response()->json(['id' => $user->id, 'new_token' => csrf_token()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdateUser(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required|numeric',
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255',
                    'password' => 'required|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return Response()->json(['error' => true], 403);
        }
        $user = User::findOrFail($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        return Response()->json(['id' => $user->id, 'new_token' => csrf_token()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function DeleteUser(Request $request) {
        $user = User::findOrFail($request->id);
        $user->delete();
        return Response()->json(['new_token' => csrf_token()],200);
    }

}
