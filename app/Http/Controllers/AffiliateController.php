<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Affiliate;
use Validator;
use Illuminate\Http\Response;

class AffiliateController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $affiliates = Affiliate::all();
        return view('affiliate', compact('affiliates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addAffiliate(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|string|min:4',
                    'company' => 'required|string|min:4',
                    'url' => 'required|string|min:4',
        ]);
        if ($validator->fails()) {
            return Response()->json(['error' => true], 403);
        }
        //Saving the multiple page
        $Affiliate = new Affiliate;
        $Affiliate->name = $request->name;
        $Affiliate->url = $request->url;
        $Affiliate->company = $request->company;
        $Affiliate->save();
        return Response()->json(['id' => $Affiliate->id, 'new_token' => csrf_token()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdateAffiliate(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required|numeric',
                    'name' => 'required|string|min:4',
                    'company' => 'required|string|min:4',
                    'url' => 'required|string|min:4',
        ]);
        if ($validator->fails()) {
            return Response()->json(['error' => true], 403);
        }
        $id = $request->id;
        //Saving the multiple page
        $Affiliate = Affiliate::findOrFail($id);
        $Affiliate->name = $request->name;
        $Affiliate->url = $request->url;
        $Affiliate->company = $request->company;
        $Affiliate->save();
        return Response()->json(['id' => $Affiliate->id, 'new_token' => csrf_token()],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function DeleteAffiliate(Request $request) {
        $Affiliate = Affiliate::findOrFail($request->id);
        $Affiliate->delete();
        return Response()->json(['new_token' => csrf_token()],200);
    }

}
